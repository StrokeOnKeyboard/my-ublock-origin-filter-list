# A uBlock Origin filter list
This project includes dynamic filters list for uBlock Origin.

A uBlock origin Filter list
To enable this filter list Download `uBlock Origin Rules.txt` and go to
`uBlock Origin > Dashboard > My Rules > Import from file` and select `uBlock Origin Rules.txt`
and click on `Commit`.
Be sure to enable `I am an advanced user` in the Dashboard.
